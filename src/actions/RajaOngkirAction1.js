import axios from 'axios';
import {
  API_HEADER_RAJAONGKIR,
  API_RAJAONGKIR,
  API_TIMEOUT,
  API_HEADER_RAJAONGKIR_COST,
  ORIGIN_CITY
} from '../utils/constant';
import {dispatchError, dispatchLoading, dispatchSuccess} from '../utils';

export const GET_PROVINSI = 'GET_PROVINSI';
export const GET_KOTA = 'GET_KOTA';
export const GET_KOTA_DETAIL = 'GET_KOTA_DETAIL';
export const POST_ONGKIR = 'POST_ONGKIR'

export const getProvinsiList = () => {
  return (dispatch) => {
    // LOADING
    dispatchLoading(dispatch, GET_PROVINSI);

    axios({
      method: 'get',
      url: API_RAJAONGKIR + 'province',
      timeout: API_TIMEOUT,
      headers: API_HEADER_RAJAONGKIR,
    })
      .then((response) => {
        if (response.status !== 200) {
          // ERROR
          dispatchError(dispatch, GET_PROVINSI, response);
        } else {
          //SUKSES
          dispatchSuccess(
            dispatch,
            GET_PROVINSI,
            response.data ? response.data.rajaongkir.results : [],
          );
        }
      })
      .catch((error) => {
        // ERROR
        dispatchError(dispatch, GET_PROVINSI, error);

        alert(error);
      });
  };
};

export const getKotaList = (provinsi_id) => {
  return (dispatch) => {
    // LOADING
    dispatchLoading(dispatch, GET_KOTA);

    axios({
      method: 'get',
      url: API_RAJAONGKIR + 'city?province=' + provinsi_id,
      timeout: API_TIMEOUT,
      headers: API_HEADER_RAJAONGKIR,
    })
      .then((response) => {
        if (response.status !== 200) {
          // ERROR
          dispatchError(dispatch, GET_KOTA, response);
        } else {
          //SUKSES
          dispatchSuccess(
            dispatch,
            GET_KOTA,
            response.data ? response.data.rajaongkir.results : [],
          );
        }
      })
      .catch((error) => {
        // ERROR
        dispatchError(dispatch, GET_KOTA, error);

        alert(error);
      });
  };
};

export const getKotaDetail = (kota_id) => {
  return (dispatch) => {
    // LOADING
    dispatchLoading(dispatch, GET_KOTA_DETAIL);

    axios({
      method: 'get',
      url: API_RAJAONGKIR + 'city?id=' + kota_id,
      timeout: API_TIMEOUT,
      headers: API_HEADER_RAJAONGKIR,
    })
      .then((response) => {
        if (response.status !== 200) {
          // ERROR
          dispatchError(dispatch, GET_KOTA_DETAIL, response);
        } else {
          //SUKSES
          dispatchSuccess(
            dispatch,
            GET_KOTA_DETAIL,
            response.data ? response.data.rajaongkir.results : [],
          );
        }
      })
      .catch((error) => {
        // ERROR
        dispatchError(dispatch, GET_KOTA_DETAIL, error);

        alert(error);
      });
  };
};

export const postOngkir = (data, ekspedisi) => {

  return (dispatch) => {
    dispatchLoading(dispatch, POST_ONGKIR);

    const formData = new URLSearchParams();
    formData.append('origin', ORIGIN_CITY);
    formData.append('destination', data.profile.kota);
    formData.append('weight', data.totalBerat < 1 ? 1000 : data.totalBerat*1000);
    formData.append('courier', ekspedisi.kurir);

    //console.log("Cek data form : ",formData)

    axios({
      method: 'POST',
      url: API_RAJAONGKIR+'cost',
      timeout: API_TIMEOUT,
      headers: API_HEADER_RAJAONGKIR_COST,
      data: formData
      // data: {origin:'349', destination:'114', weight:1000, courier:'tiki'}
    })
      .then((response) => {
        if (response.status !== 200) {
          // ERROR
          dispatchError(dispatch, POST_ONGKIR, response);
        } else {

          const ongkirs = response.data.rajaongkir.results[0].costs;
          const ongkirYangDipilih = ongkirs
          .filter((ongkir) => ongkir.service === ekspedisi.service).map((filterOngkir) => {
            return filterOngkir
          });
          //SUKSES
          dispatchSuccess(
            dispatch,
            POST_ONGKIR,
            ongkirYangDipilih[0],
          );
          console.log(response);
        }
      })
      .catch((error) => {
        // ERROR
        dispatchError(dispatch, POST_ONGKIR, error);
        alert(error);
        
        console.log(error.response);
      })

  }
}


// import axios from "axios";
// import { dispatchError, dispatchLoading, dispatchSuccess } from "../utils";
// import { API_HEADER_RAJAONGKIR, API_HEADER_RAJAONGKIR_COST, ORIGIN_CITY, 
//     API_RAJAONGKIR, API_TIMEOUT } from "../utils/constant";

// export const GET_PROVINSI = "GET_PROVINSI";
// export const GET_KOTA = "GET_KOTA";
// export const GET_KOTA_DETAIL = "GET_KOTA_DETAIL";
// export const POST_ONGKIR = "POST_ONGKIR";

// export const getProvinsiList = () => {
//     return (dispatch) => {

//         // LOADING
//         dispatchLoading(dispatch, GET_PROVINSI)
//         // dispatch({
//         //     type: GET_PROVINSI,
//         //     payload: {
//         //         loading: true,
//         //         data: false,
//         //         errorMessage: false,
//         //     }
//         // })

//         axios({
//             method: 'GET',
//             url: API_RAJAONGKIR + "province",
//             timeout: API_TIMEOUT,
//             headers: API_HEADER_RAJAONGKIR
//         }).then((response) => {
//             //console.log("Cek response axios :", response);
//             if (response.status !== 200) {

//                 // ERROR RESPONSE
//                 dispatchError(dispatch, GET_PROVINSI, error.message)
//                 // dispatch({
//                 //     type: GET_PROVINSI,
//                 //     payload: {
//                 //         loading: false,
//                 //         data: false,
//                 //         errorMessage: response,
//                 //     }
//                 // })
//             } else {

//                 //console.log("Cek response sukses :", response);
//                 // SUCCESS RESPONSE 

//                 dispatchSuccess(dispatch, GET_PROVINSI, response.data ? response.data.rajaongkir.results : [])

//                 // dispatch({
//                 //     type: GET_PROVINSI,
//                 //     payload: {
//                 //         loading: false,
//                 //         data: response.data ? response.data.rajaongkir.results : [],
//                 //         errorMessage: false,
//                 //     }
//                 // })
//             }
//         }).catch((error) => {

//             // ERROR MESSAGE
//             dispatchError(dispatch, GET_PROVINSI, error.message)

//             // dispatch({
//             //     type: GET_PROVINSI,
//             //     payload: {
//             //         loading: false,
//             //         data: false,
//             //         errorMessage: error,
//             //     }
//             // })
//             alert(error)
//         })
//     }
// }

// export const getKotaList = (provinsi_id) => {
//     return (dispatch) => {

//         // LOADING
//         dispatchLoading(dispatch, GET_KOTA)

//         // dispatch({
//         //     type: GET_KOTA,
//         //     payload: {
//         //         loading: true,
//         //         data: false,
//         //         errorMessage: false,
//         //     }
//         // })

//         axios({
//             method: 'GET',
//             url: API_RAJAONGKIR + "city?province=" + provinsi_id,
//             timeout: API_TIMEOUT,
//             headers: API_HEADER_RAJAONGKIR
//         }).then((response) => {
//             //console.log("Cek response axios :", response);
//             if (response.status !== 200) {

//                 // ERROR RESPONSE
//                 dispatchError(dispatch, GET_KOTA, error.message)
//                 // dispatch({
//                 //     type: GET_KOTA,
//                 //     payload: {
//                 //         loading: false,
//                 //         data: false,
//                 //         errorMessage: response,
//                 //     }
//                 // })
//             } else {

//                 //console.log("Cek response sukses :", response);
//                 // SUCCESS RESPONSE 
//                 dispatchSuccess(dispatch, GET_KOTA, response.data ? response.data.rajaongkir.results : [])

//                 // dispatch({
//                 //     type: GET_KOTA,
//                 //     payload: {
//                 //         loading: false,
//                 //         data: response.data ? response.data.rajaongkir.results : [],
//                 //         errorMessage: false,
//                 //     }
//                 // })
//             }
//         }).catch((error) => {

//             // ERROR MESSAGE
//             dispatchError(dispatch, GET_KOTA, error.message)

//             // dispatch({
//             //     type: GET_KOTA,
//             //     payload: {
//             //         loading: false,
//             //         data: false,
//             //         errorMessage: error,
//             //     }
//             // })

//             alert(error)
//         })
//     }
// }

// export const getKotaDetail = (kota_id) => {
//     return (dispatch) => {

//         // LOADING
//         dispatchLoading(dispatch, GET_KOTA_DETAIL)

//         axios({
//             method: 'GET',
//             url: API_RAJAONGKIR + "city?id=" + kota_id,
//             timeout: API_TIMEOUT,
//             headers: API_HEADER_RAJAONGKIR
//         }).then((response) => {

//             if (response.status !== 200) {

//                 // ERROR RESPONSE
//                 dispatchError(dispatch, GET_KOTA_DETAIL, error.message)
//             } else {

//                 // SUCCESS RESPONSE 
//                 dispatchSuccess(dispatch, GET_KOTA_DETAIL, response.data ? response.data.rajaongkir.results : [])
//             }
//         }).catch((error) => {

//             // ERROR MESSAGE
//             dispatchError(dispatch, GET_KOTA_DETAIL, error.message)
//             alert(error)
//         })
//     }
// }

// export const postOngkir = (data, ekspedisi) => {
//     //console.log("get data city user : ", data.profile);
//     console.log("get data ekspedisi :", ekspedisi);
//     return (dispatch) => {
//         dispatchLoading(dispatch, POST_ONGKIR);

//         const formData = new URLSearchParams();
//         formData.append('origin', ORIGIN_CITY);
//         formData.append('destination', data.profile.kota)
//         formData.append('weight', data.totalBerat < 1 ? 1000 : data.totalBerat * 1000)
//         formData.append('courier', ekspedisi.kurir);

//         console.log("Content form data : ",formData);
//         axios({
//             method: 'POST',
//             url: API_RAJAONGKIR + 'cost',
//             timeout: API_TIMEOUT,
//             headers: API_HEADER_RAJAONGKIR_COST,
//             data: formData,
//             //data: {origin:'501',destination:'387',weight:1500,courier:'jne'}
//         })
//             .then((response) => {
//                 if (response.status == 200) {
//                     // ERROR RESPONSE
//                     dispatchError(dispatch, POST_ONGKIR, response)
//                 } else {
//                     const ongkirs = response.data.rajaongkir.results.costs;
//                     const ongkirPilih = ongkirs.filter((ongkir) =>
//                         ongkir.service === ekspedisi.service).map((filterOngkir) => {
//                             return filterOngkir
//                         });
//                     // SUCCESS RESPONSE 
//                     dispatchSuccess(dispatch, POST_ONGKIR, ongkirPilih)
//                 }
//             })
//             .catch((error) => {
//                 dispatchError(dispatch, POST_ONGKIR, error.message)
//                 alert(error)
//             })
//     }
// }
