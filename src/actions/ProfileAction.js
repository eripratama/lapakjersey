import { Alert } from "react-native";
import FIREBASE from "../config/FIREBASE";
import { dispatchError, dispatchLoading, dispatchSuccess, storeData } from "../utils";

export const UPDATE_PROFILE = "UPDATE_PROFILE";
export const CHANGE_PASSWORD = "CHANGE_PASSWORD"

export const updateProfile = (data) => {

    return (dispatch) => {

        // LOADING
        dispatchLoading(dispatch, UPDATE_PROFILE);
        // dispatch({
        //     type: UPDATE_PROFILE,
        //     payload: {
        //         loading: true,
        //         data: false,
        //         errorMessage: false
        //     }
        // })


        const dataBaru = {
            uid: data.uid,
            nama: data.nama,
            alamat: data.alamat,
            nohp: data.nohp,
            kota: data.kota,
            provinsi: data.provinsi,
            email: data.email,
            status: 'user',
            avatar: data.updateAvatar ? data.avatarForDB : data.avatarLama
        }

        // console.log(dataBaru)

        FIREBASE.database()
            .ref('users/' + dataBaru.uid)
            .update(dataBaru)
            .then((response) => {
                // SUCCESS
                dispatchSuccess(dispatch, UPDATE_PROFILE, response ? response : [])

                // dispatch({
                //     type: UPDATE_PROFILE,
                //     payload: {
                //         loading: false,
                //         data: response ? response : [],
                //         errorMessage: false
                //     }
                // })

                // TO LOCAL STORAGE
                storeData('user', dataBaru);
            })
            .catch((error) => {
                dispatchError(dispatch, UPDATE_PROFILE, error.message)
                alert(error.message)
                // dispatch({
                //     type: UPDATE_PROFILE,
                //     payload: {
                //         loading: false,
                //         data: false,
                //         errorMessage: error.message,
                //     }
                // })
            })
    }
}

export const changePassword = (data) => {

    return (dispatch) => {
        dispatchLoading(dispatch, CHANGE_PASSWORD);

        // CEK DATA EMAIL & PASSWORD DENGAN DATA USER YANG LOGIN
        FIREBASE.auth()
            .signInWithEmailAndPassword(data.email, data.password)
            .then((response) => {
                // RESPONSE SUKSES
                var user = FIREBASE.auth().currentUser;
                user
                    .updatePassword(data.newPassword)
                    .then(function () {
                        // UPDATE SUCCESS
                        dispatchSuccess(dispatch, CHANGE_PASSWORD, "Berhasil update password");
                        Alert.alert('Notifikasi', 'Berhasil ganti password')
                    })
                    .catch(function (error) {
                        dispatchError(dispatch, CHANGE_PASSWORD, error);
                    })

            }).catch((error) => {
                dispatchError(dispatch, CHANGE_PASSWORD, error.message)
                alert(error.message)
            })
    }
}