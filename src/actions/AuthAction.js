import FIREBASE from '../config/FIREBASE'
import { dispatchError, dispatchLoading, dispatchSuccess, storeData } from '../utils'

export const LOGIN_USER = "LOGIN_USER";
export const REGISTER_USER = "REGISTER_USER";

export const loginUser = (email, password) => {

    return (dispatch) => {

        // LOADING
        dispatchLoading(dispatch, LOGIN_USER)

        // dispatch({
        //     type: LOGIN_USER,
        //     payload: {
        //         loading: true,
        //         data: false,
        //         errorMessage: false
        //     }
        // })

        FIREBASE
            .auth()
            .signInWithEmailAndPassword(email, password)
            .then((success) => {
                // SIGNED IN
                FIREBASE
                    .database().ref('/users/' + success.user.uid)
                    .once('value')
                    .then((resDB) => {

                        if (resDB.val()) {
                            // SUKSES
                            dispatchSuccess(dispatch, LOGIN_USER, resDB.val())

                            // dispatch({
                            //     type: LOGIN_USER,
                            //     payload: {
                            //         loading: false,
                            //         data: resDB.val(),
                            //         errorMessage: false
                            //     }
                            // })

                            // LOCAL STORAGE
                            storeData('user', resDB.val())
                        } else {

                            dispatchError(dispatch, LOGIN_USER, error.message)
                            // dispatch({
                            //     type: LOGIN_USER,
                            //     payload: {
                            //         loading: false,
                            //         data: false,
                            //         errorMessage: "Data tidak ada"
                            //     }
                            // })
                            alert("Data tidak ditemukan")
                        }
                    })
            })
            .catch((error) => {

                // error sign in
                dispatch({
                    type: LOGIN_USER,
                    payload: {
                        loading: false,
                        data: false,
                        errorMessage: error.message
                    }
                })

                alert(error.message)
            })
    }
}



export const registerUser = (data, password) => {
    return (dispatch) => {

        // LOADING
        dispatch({
            type: REGISTER_USER,
            payload: {
                loading: true,
                data: false,
                errorMessage: false
            }
        })

        FIREBASE
            .auth()
            .createUserWithEmailAndPassword(data.email, password)
            .then((success) => {
                // AMBIL UID BUAT DATA BARU (data+uid)
                const dataBaru = {
                    ...data,
                    uid: success.user.uid
                }

                // SIMPAN KE DB REALTIME FIREBASE
                FIREBASE.database()
                    .ref('users/' + success.user.uid)
                    .set(dataBaru);

                // SUKSES
                dispatch({
                    type: REGISTER_USER,
                    payload: {
                        loading: false,
                        data: dataBaru,
                        errorMessage: false
                    }
                })

                // LOCAL STORAGE
                storeData('user', dataBaru)


            })
            .catch((error) => {
                // ..
                dispatch({
                    type: REGISTER_USER,
                    payload: {
                        loading: false,
                        data: false,
                        errorMessage: error.message
                    }
                })

                alert(error.message)
            });
    }
}