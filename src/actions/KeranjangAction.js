import FIREBASE from "../config/FIREBASE";
import { dispatchError, dispatchLoading, dispatchSuccess, storeData } from "../utils";

export const MASUK_KERANJANG = "MASUK_KERANJANG";
export const GET_LIST_KERANJANG = "GET_LIST_KERANJANG";
export const DELETE_KERANJANG = "DELETE_KERANJANG";

export const getListKeranjang = (id) => {
    return (dispatch) => {
        dispatchLoading(dispatch, GET_LIST_KERANJANG)

        FIREBASE.database()
            .ref('keranjangs/' + id)
            .once('value', (querySnapshot) => {
                // getSuccess
                // console.log("Cek response data :",querySnapshot.val());
                let data = querySnapshot.val();
                //let dataItem = { ...data };

                dispatchSuccess(dispatch, GET_LIST_KERANJANG, data)
            })
            .catch((error) => {
                dispatchError(dispatch, GET_LIST_KERANJANG, error)
                alert(error)
            })
    }
}

export const masukKeranjang = (data) => {
    return (dispatch) => {
        dispatchLoading(dispatch, MASUK_KERANJANG)

        FIREBASE.database()
            .ref('keranjangs/' + data.uid)
            .once('value', (querySnapshot) => {

                if (querySnapshot.val()) {
                    // update keranjang utama
                    const keranjangUtama = querySnapshot.val()
                    const beratBaru = parseInt(data.jumlah) * parseFloat(data.itemJersey.berat)
                    const hargaBaru = parseInt(data.jumlah) * parseInt(data.itemJersey.harga)

                    FIREBASE.database()
                        .ref('keranjangs')
                        .child(data.uid)
                        .update({
                            totalHarga: keranjangUtama.totalHarga + hargaBaru,
                            totalBerat: keranjangUtama.totalBerat + beratBaru
                        })

                        .then((response) => {
                            // simpan ke keranjang detail
                            dispatch(masukKeranjangDetail(data))
                        })
                        .catch((error) => {
                            dispatchError(dispatch, MASUK_KERANJANG, error)
                            alert(error)
                        })
                } else {

                    // simpan data keranjang utama
                    const keranjangUtama = {
                        user: data.uid,
                        tanggal: new Date().toDateString(),
                        totalHarga: parseInt(data.jumlah) * parseInt(data.itemJersey.harga),
                        totalBerat: parseInt(data.jumlah) * parseFloat(data.itemJersey.berat)
                    }

                    FIREBASE.database()
                        .ref('keranjangs')
                        .child(data.uid)
                        .set(keranjangUtama)

                        .then((response) => {
                            // simpan ke keranjang detail
                            dispatch(masukKeranjangDetail(data))
                        })
                        .catch((error) => {
                            dispatchError(dispatch, MASUK_KERANJANG, error)
                            alert(error)
                        })

                }
            })
            .catch((error) => {
                dispatchError(dispatch, MASUK_KERANJANG, error)
                alert(error)
            })
    }
}

export const masukKeranjangDetail = (data) => {
    return (dispatch) => {
        const pesanans = {
            product: data.itemJersey,
            jumlahPesan: data.jumlah,
            totalHarga: parseInt(data.jumlah) * parseInt(data.itemJersey.harga),
            totalBerat: parseInt(data.jumlah) * parseFloat(data.itemJersey.berat),
            keterangan: data.keterangan,
            ukuran: data.ukuran
        }

        FIREBASE.database()
            .ref('keranjangs/' + data.uid)
            .child('pesanans')
            .push(pesanans)
            .then((response) => {
                dispatchSuccess(dispatch, MASUK_KERANJANG, response ? response : [])
            }).catch((error) => {
                dispatchError(dispatch, MASUK_KERANJANG, error)
                alert(error)

            })
    }
}

export const deleteKeranjang = (id, keranjangUtama, keranjang) => {
    return (dispatch) => {
        dispatchLoading(dispatch, DELETE_KERANJANG);

        const totalHargaBaru = keranjangUtama.totalHarga - keranjang.totalHarga;
        const totalBeratBaru = keranjangUtama.totalBerat - keranjang.totalBerat;

        if (totalHargaBaru === 0) {
            // HAPUS KERANJANG UTAMA DAN DETAIL
            FIREBASE.database()
                .ref('keranjangs')
                .child(keranjangUtama.user)
                .remove()
                .then((response) => {
                    dispatchSuccess(dispatch, DELETE_KERANJANG, "Item berhasil di hapus")
                }).catch((error) => {
                    dispatchError(dispatch, DELETE_KERANJANG, error);
                    alert(error);
                })
        } else {
            // UPDATE TOTAL HARGA & TOTAL BERAT KERANJANG UTAMA
            FIREBASE.database()
                .ref('keranjangs')
                .child(keranjangUtama.user)
                .update({
                    totalBerat: totalBeratBaru,
                    totalHarga: totalHargaBaru
                })
                .then((response) => {
                    // HAPUS PESANAN/ KERANJANG DETAIL
                    dispatch(deleteKeranjangDetail(id, keranjangUtama));
                }).catch((error) => {
                    dispatchError(dispatch, DELETE_KERANJANG, error)
                    alert(error);
                })
        }
    }
}

export const deleteKeranjangDetail = (id, keranjangUtama) => {
    return (dispatch) => {
        FIREBASE.database()
            .ref('keranjangs/' + keranjangUtama.user)
            .child('pesanans')
            .child(id)
            .remove()
            .then((response) => {
                dispatchSuccess(dispatch, DELETE_KERANJANG, "Item keranjang berhasil di hapus")
            }).catch((error) => {
                dispatchError(dispatch, DELETE_KERANJANG, error)
                alert(error)
            })
    }

}