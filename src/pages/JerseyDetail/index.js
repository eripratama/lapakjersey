import React, { Component } from 'react'
import { Text, StyleSheet, View, Image, Alert } from 'react-native'
import { colors, fonts, formatPrice, getData, responsiveHeight, responsiveWidth } from '../../utils'
import { RFValue } from 'react-native-responsive-fontsize'
import { heightMobileUI } from '../../utils/constant'
import { CardLiga, Inputan, Jarak, JerseySlider, Pilihan, Tombol } from '../../components'
import { connect } from 'react-redux'
import { getDetailLiga } from '../../actions/LigaAction'
import { masukKeranjang } from '../../actions/KeranjangAction'

class JerseyDetail extends Component {

    constructor(props) {
        super(props)

        this.state = {
            itemJersey: this.props.route.params.itemJersey,
            images: this.props.route.params.itemJersey.gambar,
            jumlah: "",
            ukuran: "",
            keterangan: "",
            uid: ""
        }
    }

    componentDidMount() {
        const { itemJersey } = this.state
        this.props.dispatch(getDetailLiga(itemJersey.liga))
    }

    componentDidUpdate(prevProps) {
        const { saveKeranjangResult } = this.props

        if (saveKeranjangResult && prevProps.saveKeranjangResult !== saveKeranjangResult) {
            
            this.props.navigation.navigate("Keranjang")
        }
    }

    masukKeranjang = () => {
        const { keterangan, jumlah, ukuran } = this.state
        getData('user').then((res) => {

            if (res) {
                // SIMPAN uid local storage ke state
                this.setState({
                    uid: res.uid
                })
                // validasi form 
                if (jumlah && keterangan && ukuran) {
                    // connect ke action (KeranjangAction / masukKeranjang)
                    this.props.dispatch(masukKeranjang(this.state))
                } else {
                    Alert.alert('Error', 'Ada data yang masih kosong!!!');
                }
            } else {
                Alert.alert('Error', 'Silahkan login terlebih dahulu');
                this.props.navigation.replace('Login')
            }
        })
    }

    render() {
        const { navigation, getDetailLigaResult, saveKeranjangLoading } = this.props
        const { itemJersey, images, jumlah, keterangan, ukuran } = this.state
        // CEK PARAMETER YANG DIKIRIM DARI ITEM CARD
        // console.log("Parameter : ",this.props.route.params)
        return (
            <View style={styles.page}>
                <View style={styles.button}>
                    <Tombol icon="arrow-left" padding={7} onPress={() => navigation.goBack()} />
                </View>

                <JerseySlider images={images} />
                <View style={styles.container}>

                    <View style={styles.liga}>
                        <CardLiga liga={getDetailLigaResult} navigation={navigation} id={itemJersey.liga} />
                    </View>

                    <View style={styles.desc}>
                        <Text style={styles.nama}>{itemJersey.nama}</Text>
                        <Text style={styles.harga}>Harga : Rp. {formatPrice(itemJersey.harga)}
                            {/* : {jumlah} | {ukuran} | {keterangan} */}
                        </Text>

                        <View style={styles.garis} />

                        <View style={styles.wrapperJenisBerat}>
                            <Text style={styles.jenisBerat}>Jenis : {itemJersey.jenis}</Text>
                            <Text style={styles.jenisBerat}>Berat : {itemJersey.berat}</Text>
                        </View>

                        <View style={styles.wrapperInput}>
                            <Inputan label="Jumlah / Qty"
                                width={responsiveWidth(166)}
                                height={responsiveHeight(43)}
                                fontSize={13}
                                value={jumlah}
                                onChangeText={(jumlah) => this.setState({ jumlah })}
                                keyboardType="number-pad"
                            />
                            <Pilihan
                                label="Pilih Ukuran"
                                width={responsiveWidth(166)}
                                height={responsiveHeight(43)}
                                fontSize={12}
                                datas={itemJersey.ukuran}
                                selectedValue={ukuran}
                                onValueChange={(ukuran) => this.setState({ ukuran })}
                            />
                        </View>
                        <Inputan label="keterangan" textarea placeholder="Masukkan nama dan no punggung untuk kustom jersey"
                            fontSize={13}
                            value={keterangan}
                            onChangeText={(keterangan) => this.setState({ keterangan })}
                        />
                        <Jarak height={15} />

                        <Tombol
                            title="Tambah ke Keranjang"
                            type="textIcon"
                            icon="cart"
                            padding={responsiveHeight(17)}
                            fontSize={18}
                            onPress={() => this.masukKeranjang()}
                            loading={saveKeranjangLoading}
                        />
                    </View>
                </View>
            </View>
        )
    }
}

const mapStateToProps = (state) => ({
    getDetailLigaResult: state.LigaReducer.getDetailLigaResult,

    saveKeranjangLoading: state.KeranjangReducer.saveKeranjangLoading,
    saveKeranjangResult: state.KeranjangReducer.saveKeranjangResult,
    saveKeranjangError: state.KeranjangReducer.saveKeranjangError,
})

export default connect(mapStateToProps, null)(JerseyDetail)

const styles = StyleSheet.create({
    page: {
        flex: 1,
        backgroundColor: colors.primary
    },
    container: {
        position: 'absolute',
        bottom: 0,
        height: responsiveHeight(465),
        width: '100%',
        backgroundColor: colors.white,
        borderTopRightRadius: 40,
        borderTopLeftRadius: 40
    },
    button: {
        position: 'absolute',
        marginTop: 20,
        marginLeft: 30,
        zIndex: 1
    },
    desc: {
        marginHorizontal: 30
    },
    nama: {
        marginTop: 5,
        textTransform: 'capitalize',
        fontFamily: fonts.primary.bold,
        fontSize: RFValue(24, heightMobileUI)
    },
    harga: {
        fontFamily: fonts.primary.light,
        fontSize: RFValue(24, heightMobileUI)
    },
    liga: {
        alignItems: 'flex-end',
        marginRight: 30,
        marginTop: -30
    },
    garis: {
        marginVertical: 5,
        borderWidth: 0.3
    },
    wrapperJenisBerat: {
        marginBottom: 5,
        flexDirection: 'row',
    },
    jenisBerat: {
        fontSize: 13,
        fontFamily: fonts.primary.regular,
        marginRight: 30
    },
    wrapperInput: {
        flexDirection: 'row',
        justifyContent: 'space-between'
    }
})