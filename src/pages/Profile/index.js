import React, { Component } from 'react'
import { Text, StyleSheet, View, Image } from 'react-native'
import { colors, fonts, responsiveHeight, responsiveWidth, getData } from '../../utils'
import { dummyMenu, dummyProfile } from '../../data'
import { RFValue } from 'react-native-responsive-fontsize'
import { heightMobileUI } from '../../utils/constant'
import ListMenu from '../../components/besar/ListMenu'
import { DefaultImage } from '../../assets'

export default class Profile extends Component {

    constructor(props) {
        super(props)

        this.state = {
            profile: false,
            //profile: dummyProfile,
            menus: dummyMenu
        }
    }

    // componentDidMount(){
    //     this.getUserData();
    // }

    componentDidMount() {
        this._unsubscribe = this.props.navigation.addListener('focus', () => {
            console.log("Komponen terpasang");
            this.getUserData();
        });
    }

    componentWillUnmount() {
        this._unsubscribe();
    }

    getUserData = () => {
        getData('user').then(res => {
            const data = res

            if (data) {
                this.setState({
                    profile: data
                })
            } else {
                this.props.navigation.replace('Login')
            }
        })
    }

    render() {

        const { profile, menus } = this.state
        console.log("Data Profil login :", profile);
        return (
            <View style={styles.page}>
                <View style={styles.container}>
                    <Image source={profile.avatar ? { uri: profile.avatar } : DefaultImage} style={styles.avatar} />
                    {/* <Image source={profile.avatar} style={styles.avatar} /> */}
                    <View style={styles.profile}>
                        <Text style={styles.title}>{profile.nama}</Text>
                        {/* <Text style={styles.desc}>No.Hp : {profile.nomorHp}</Text>
                        <Text style={styles.desc}>{profile.alamat} {profile.kota}</Text> */}
                    </View>
                    <ListMenu menus={menus} navigation={this.props.navigation} />
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    page: {
        flex: 1,
        backgroundColor: colors.primary
    },
    profile: {
        marginTop: 10,
        alignItems: 'center'
    },
    container: {
        position: 'absolute',
        bottom: 0,
        height: responsiveHeight(680),
        width: '100%',
        backgroundColor: colors.white,
        borderTopRightRadius: 40,
        borderTopLeftRadius: 40
    },
    avatar: {
        width: responsiveWidth(150),
        height: responsiveWidth(150),
        borderRadius: 40,
        alignSelf: 'center',
        marginTop: -responsiveWidth(75)
    },
    title: {
        fontFamily: fonts.primary.bold,
        fontSize: RFValue(24, heightMobileUI)
    },
    desc: {
        fontFamily: fonts.primary.regular,
        fontSize: RFValue(18, heightMobileUI)
    },

})