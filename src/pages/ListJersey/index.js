import React, { Component } from 'react'
import { Text, StyleSheet, View, ScrollView } from 'react-native'
import { BannerSlider, HeaderComponent, ListJerseys, ListLiga } from '../../components/besar'
import { colors, fonts } from '../../utils'
import { dummyJersey, dummyLiga } from '../../data'
import { Jarak, Tombol } from '../../components'
import { connect } from 'react-redux'
import { getListLiga } from '../../actions/LigaAction'
import { getListJersey } from '../../actions/JerseyAction'

class ListJersey extends Component {

    componentDidMount() {

        this._unsubscribe = this.props.navigation.addListener('focus', () => {
            const { idLiga, keyword } = this.props
            this.props.dispatch(getListLiga());
            this.props.dispatch(getListJersey(idLiga, keyword));
        })
    }

    componentWillUnmount() {
        this._unsubscribe();
    }

    componentDidUpdate(prevProps) {
        const { idLiga,keyword } = this.props
        if (idLiga && prevProps.idLiga !== idLiga) {
            this.props.dispatch(getListJersey(idLiga, keyword));
        }

        if (keyword && prevProps.keyword !== keyword) {
            this.props.dispatch(getListJersey(idLiga, keyword));
        }
    }

    render() {
        console.log("Cek Liga by id :", this.props.idLiga);
        //const { liga, jersey } = this.state
        const { navigation, namaLiga, keyword } = this.props
        return (
            <View style={styles.page}>
                <ScrollView showsVerticalScrollIndicator={false}>
                    <HeaderComponent navigation={navigation} page="ListJersey" />

                    <View style={styles.listliga}>
                        <ListLiga navigation={navigation} />
                    </View>

                    <View style={styles.listjersey}>
                        {keyword ? (
                            <Text style={styles.label}>
                                Cari : <Text style={styles.boldlabel}> {keyword} </Text>

                            </Text>
                        ) : (

                            <Text style={styles.label}>
                                Pilih <Text style={styles.boldlabel}> Jersey </Text>
                                {namaLiga ? namaLiga : ""}
                            </Text>
                        )}

                        <ListJerseys navigation={navigation} />
                    </View>
                    <Jarak height={100} />
                </ScrollView>
            </View>
        )
    }
}

const mapStateToProps = (state) => ({
    idLiga: state.JerseyReducer.idLiga,
    namaLiga: state.JerseyReducer.namaLiga,
    keyword: state.JerseyReducer.keyword
})

export default connect(mapStateToProps, null)(ListJersey)

const styles = StyleSheet.create({
    page: { flex: 1, backgroundColor: colors.white },
    listliga: {
        marginHorizontal: 30,
        marginTop: -30,
    },
    listjersey: {
        marginHorizontal: 30,
        marginTop: 10,
    },
    label: {
        fontSize: 18,
        fontFamily: fonts.primary.regular
    },
    boldlabel: {
        fontSize: 18,
        fontFamily: fonts.primary.bold
    }
})
