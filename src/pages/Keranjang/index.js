import React, { Component } from 'react'
import { Text, StyleSheet, View } from 'react-native'
import { connect } from 'react-redux'
import ListKeranjang from '../../components/besar/ListKeranjang'
import { Tombol } from '../../components/kecil'
import { dummyOrder } from '../../data'
import { colors, fonts, formatPrice, getData, responsiveHeight } from '../../utils'
import { getListKeranjang } from '../../actions/KeranjangAction'

class Keranjang extends Component {

    componentDidMount() {
        getData('user').then((res) => {
            if (res) {
                // SUDAH LOGIN
                this.props.dispatch(getListKeranjang(res.uid))
            } else {
                // BELUM LOGIN
                this.props.navigation.replace("Login")
            }
        })
    }

    componentDidUpdate(prevProps) {
        const { deleteKeranjangResult } = this.props

        if (deleteKeranjangResult && prevProps.deleteKeranjangResult !== deleteKeranjangResult) {
            getData('user').then((res) => {
                if (res) {
                    // SUDAH LOGIN
                    this.props.dispatch(getListKeranjang(res.uid))
                } else {
                    // BELUM LOGIN
                    this.props.navigation.replace("Login")
                }
            })
        }
    }

    render() {
        // const { order } = this.state
        //console.log("Data item keranjang", this.props.getListKeranjangResult);
        const { getListKeranjangResult } = this.props
        return (
            <View style={styles.page}>
                <ListKeranjang {...this.props} />

                <View style={styles.footer}>
                    <View style={styles.totalHarga}>
                        <Text style={styles.textBold}>Total Harga : </Text>
                        <Text style={styles.textBold}>
                            Rp. {getListKeranjangResult ?
                                formatPrice(getListKeranjangResult.totalHarga) : '-'}
                        </Text>
                        {/* <Text style={styles.textBold}>
                            Rp. {getListKeranjangResult ? getListKeranjangResult.totalHarga : '-'}
                        </Text> */}
                    </View>


                    {getListKeranjangResult ? (
                        <Tombol
                            title="CheckOut"
                            type="textIcon"
                            fontSize={18}
                            padding={responsiveHeight(15)}
                            icon="cart"
                            onPress={() => this.props.navigation.navigate('Checkout', {
                                totalHarga: getListKeranjangResult.totalHarga,
                                totalBerat: getListKeranjangResult.totalBerat
                            })}
                        />
                    ) : (
                        <Tombol
                            title="CheckOut"
                            type="textIcon"
                            fontSize={18}
                            padding={responsiveHeight(15)}
                            icon="cart"
                            disabled={true}
                        />
                    )}
                </View>
            </View>
        )
    }
}

const mapStateToProps = (state) => ({
    getListKeranjangLoading: state.KeranjangReducer.getListKeranjangLoading,
    getListKeranjangResult: state.KeranjangReducer.getListKeranjangResult,
    getListKeranjangError: state.KeranjangReducer.getListKeranjangError,

    deleteKeranjangLoading: state.KeranjangReducer.deleteKeranjangLoading,
    deleteKeranjangResult: state.KeranjangReducer.deleteKeranjangResult,
    deleteKeranjangError: state.KeranjangReducer.deleteKeranjangError,
})

export default connect(mapStateToProps, null)(Keranjang)

const styles = StyleSheet.create({
    page: {
        flex: 1,
        backgroundColor: colors.white,
     
    },

    footer: {
        paddingHorizontal: 30,
        paddingBottom: 30,
        backgroundColor: colors.white
    },
    totalHarga: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginVertical: 20
    },
    textBold: {
        fontSize: 20,
        fontFamily: fonts.primary.bold
    }
})
