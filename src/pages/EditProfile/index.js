import React, { Component } from 'react'
import { Text, StyleSheet, View, ScrollView, Image, Alert } from 'react-native'
import { dummyProfile } from '../../data'
import { colors, fonts, getData, responsiveHeight, responsiveWidth } from '../../utils'
import { Inputan, Pilihan, Tombol } from '../../components'
import { connect } from 'react-redux'
import { getKotaList, getProvinsiList } from '../../actions/RajaOngkirAction'
import { DefaultImage } from '../../assets'
import { launchImageLibrary } from 'react-native-image-picker'
import { updateProfile } from '../../actions/ProfileAction'

class EditProfile extends Component {

    constructor(props) {
        super(props)

        this.state = {
            uid: '',
            nama: '',
            email: '',
            nohp: '',
            alamat: '',
            provinsi: false,
            kota: false,
            avatar: false,
            avatarForDB: '',
            avatarLama: '',
            updateAvatar: false
        }
    }

    componentDidMount() {
        this.getUserData();
        this.props.dispatch(getProvinsiList());
    }

    componentDidUpdate(prevProps) {
        const { updateProfileResult } = this.props

        if (updateProfileResult && prevProps.updateProfileResult !== updateProfileResult) {
            Alert.alert("Notifikasi", "Data profile berhasil diperbarui")
            this.props.navigation.replace("MainApp")
        }
    }

    getUserData = () => {
        getData('user').then(res => {
            const data = res
            this.setState({
                uid: data.uid,
                nama: data.nama,
                email: data.email,
                alamat: data.alamat,
                nohp: data.nohp,
                kota: data.kota,
                provinsi: data.provinsi,
                avatar: data.avatar,
                avatarLama: data.avatar
            })
            this.props.dispatch(getKotaList(data.provinsi));
        })
    }

    ubahProvinsi = (provinsi) => {
        this.setState({
            provinsi: provinsi
        })
        this.props.dispatch(getKotaList(provinsi));
    }

    getImage = () => {
        launchImageLibrary({
            quality: 1, maxWidth: 500, maxHeight: 500,
            selectionLimit: 1, cameraType: 'front', includeBase64: true
        },
            (response) => {
                console.log("Cek response :", response)
                if (response.didCancel || response.errorCode || response.errorMessage) {
                    Alert.alert("Error", "File gambar gagal upload")
                } else {
                    const source = response.assets[0].uri;
                    const fileString = `data:${response.assets[0].type};base64,${response.assets[0].base64}`;

                    console.log("Cek data gambar : ", source);
                    this.setState({
                        avatar: source,
                        avatarForDB: fileString,
                        updateAvatar: true
                    })
                }
            });
    };

    onSubmit = () => {
        const { nama, email, alamat, nohp, provinsi, kota, avatar, avatarForDB, avatarLama } = this.state
        if (nama && nohp && alamat && provinsi && kota) {
            // DISPATCH update

            this.props.dispatch(updateProfile(this.state))

        } else {
            Alert.alert("Error", "Data belum lengkap")
        }
    }

    render() {
        const { dataProvinsi, dataKota, profile, nama, email, alamat, nohp, provinsi, kota, avatar, avatarForDB, avatarLama } = this.state
        const { getKotaResult, getProvinsiResult, updateProfileLoading, updateProfileError, updateProfileResult } = this.props

        return (
            <View style={styles.pages}>
                <ScrollView showsVerticalScrollIndicator={false}>
                    <Inputan label="Nama" value={nama} onChangeText={(nama) => this.setState({ nama })} />
                    <Inputan label="Email" value={email} disabled />
                    <Inputan label="No Handphone" value={nohp} keyboardType="number-pad" onChangeText={(nohp) => this.setState({ nohp })} />
                    <Inputan label="Alamat" value={alamat} textarea onChangeText={(alamat) => this.setState({ alamat })} />

                    <Pilihan label="Provinsi"
                        datas={getProvinsiResult ? getProvinsiResult : []}
                        selectedValue={provinsi}
                        onValueChange={(provinsi) => this.ubahProvinsi(provinsi)}
                    />

                    <Pilihan label="Kota/Kab"
                        datas={getKotaResult ? getKotaResult : []}
                        selectedValue={kota}
                        onValueChange={(kota) => this.setState({ kota: kota })}
                    />

                    <View style={styles.inputFoto}>
                        <Text style={styles.label}>Foto Profil : </Text>
                        <View style={styles.wrapperUpload}>
                            <Image
                                source={avatar ? { uri: avatar } : DefaultImage}
                                style={styles.foto} />

                            <View styles={styles.tombolChangeFoto}>
                                <Tombol title="Change Photo" type="text"
                                    onPress={() => this.getImage()}
                                    padding={7} />
                            </View>
                        </View>
                    </View>

                    <View style={styles.submit}>
                        <Tombol
                            onPress={() => this.onSubmit()}
                            loading={updateProfileLoading}
                            title="submit" type="text" padding={responsiveHeight(15)} fontSize={18} />
                    </View>
                </ScrollView>
            </View>
        )
    }
}

const mapStatetoProps = (state) => ({
    getProvinsiResult: state.RajaOngkirReducer.getProvinsiResult,
    getKotaResult: state.RajaOngkirReducer.getKotaResult,

    updateProfileLoading: state.ProfileReducer.updateProfileLoading,
    updateProfileResult: state.ProfileReducer.updateProfileResult,
    updateProfileError: state.ProfileReducer.updateProfileError
})

export default connect(mapStatetoProps, null)(EditProfile)

const styles = StyleSheet.create({
    pages: {
        flex: 1,
        backgroundColor: colors.white,
        paddingHorizontal: 30,
        paddingTop: 10,
    },
    inputFoto: {
        marginTop: 20
    },
    label: {
        fontFamily: fonts.primary.regular,
        fontSize: 18
    },
    foto: {
        width: responsiveWidth(150),
        height: responsiveHeight(150),
        borderRadius: 40
    },
    wrapperUpload: {
        flexDirection: 'row',
        marginTop: 10,
        alignItems: 'center'
    },
    tombolChangeFoto: {
        marginLeft: 20,
        flex: 1,
    },
    submit: {
        marginVertical: 30
    }
})
