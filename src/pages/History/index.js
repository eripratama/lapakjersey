import React, { Component } from 'react'
import { Text, StyleSheet, View } from 'react-native'
import { connect } from 'react-redux'
import { getListHistory } from '../../actions/HistoryAction'
import { ListHistory } from '../../components'
import { dummyOrder } from '../../data'
import { colors, getData } from '../../utils'

class History extends Component {

    componentDidMount(){
        getData('user').then((res) => {
            const data = res;

            if (!data) {
                this.props.navigation.replace('Login')
            } else {
                this.props.dispatch(getListHistory(data.uid))
            }
        })
    }

    render() {
        return (
            <View style={styles.pages}>
                <ListHistory navigation={this.props.navigation}/>
            </View>
        )
    }
}


export default connect()(History)

const styles = StyleSheet.create({
    pages:{
        backgroundColor:colors.white,
        flex:1,
    }
})
