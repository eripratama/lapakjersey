import React, { Component } from 'react'
import { Text, StyleSheet, View, ScrollView } from 'react-native'
import { BannerSlider, HeaderComponent, ListJerseys, ListLiga } from '../../components/besar'
import { colors, fonts } from '../../utils'
import { dummyJersey, dummyLiga } from '../../data'
import { Jarak, Tombol } from '../../components'

import { connect } from 'react-redux'
import { getUser } from '../../actions/UserAction'
import { getListLiga } from '../../actions/LigaAction'
import { limitJersey } from '../../actions/JerseyAction'

class Home extends Component {

    componentDidMount() {
        this._unsubscribe = this.props.navigation.addListener('focus', () => {
           this.props.dispatch(getListLiga());
           this.props.dispatch(limitJersey());
        })
    }

    componentWillUnmount() {
        this._unsubscribe();
    }

    // componentDidMount() {
    //     this.props.dispatch(getListLiga())
    // }

    render() {
        const { navigation } = this.props
        return (
            <View style={styles.page}>
                <ScrollView showsVerticalScrollIndicator={false}>
                    <HeaderComponent navigation={navigation} page="Home" />
                    <BannerSlider />

                    <View style={styles.listliga}>
                        <Text style={styles.label}>List Liga</Text>
                        {/* <Text style={styles.label}>{dataUser.nama}</Text> */}
                        <ListLiga navigation={navigation}/>
                    </View>

                    <View style={styles.listjersey}>
                        <Text style={styles.label}>Pilih <Text style={styles.boldlabel}> Jersey </Text> yang anda inginkan</Text>
                        <ListJerseys navigation={navigation} />

                        <Tombol title="Lihat Semua" type="text" padding={7} />
                    </View>

                    <Jarak height={100} />

                </ScrollView>
            </View>

        )
    }
}

const mapStatetoProps = (state) => ({
    dataUser: state.UserReducer.dataUser
})

export default connect(mapStatetoProps, null)(Home) 

const styles = StyleSheet.create({
    page: { flex: 1, backgroundColor: colors.white },
    listliga: {
        marginHorizontal: 30,
        marginTop: 10,
    },
    listjersey: {
        marginHorizontal: 30,
        marginTop: 10,
    },
    label: {
        fontSize: 18,
        fontFamily: fonts.primary.regular
    },
    boldlabel: {
        fontSize: 18,
        fontFamily: fonts.primary.bold
    }
})
