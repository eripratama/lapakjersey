import React, { Component } from 'react'
import { Text, StyleSheet, View, KeyboardAvoidingView, TouchableWithoutFeedback, Keyboard, Platform, ScrollView, Alert } from 'react-native'
import { LogoRegister2 } from '../../assets'
import { Inputan, Jarak, Pilihan, Tombol } from '../../components'
import { colors, fonts, responsiveHeight, responsiveWidth } from '../../utils'

import { connect } from 'react-redux'
import { getKotaList, getProvinsiList } from '../../actions/RajaOngkirAction'
import { registerUser } from '../../actions/AuthAction'

class Register2 extends Component {

    constructor(props) {
        super(props)

        this.state = {
            alamat: '',
            kota: false,
            provinsi: false,
        }
    }

    componentDidMount() {
        this.props.dispatch(getProvinsiList());
    }

    componentDidUpdate(prevProps){
        const { registerResult } = this.props

        if (registerResult && prevProps.registerResult !== registerResult) 
        {
            this.props.navigation.replace("MainApp")    
        }
    }

    ubahProvinsi = (provinsi) => {
        this.setState({
            provinsi: provinsi
        })
        this.props.dispatch(getKotaList(provinsi));
    }

    onContinue = () => {
        const { alamat, kota, provinsi } = this.state;

        if (alamat && kota && provinsi) {
            const data = {
                nama: this.props.route.params.nama,
                email: this.props.route.params.email,
                nohp: this.props.route.params.nohp,
                // password: this.props.route.params.password
                alamat: alamat,
                provinsi: provinsi,
                kota: kota,
                status: 'user'
            }

            // KE AUTH ACTION
            this.props.dispatch(registerUser(data, this.props.route.params.password))
            //console.log("Data Register :", data);
        } else {
            Alert.alert("Error", "Data belum lengkap")
        }
    }


    render() {
        const { alamat, kota, provinsi } = this.state;
        const { getProvinsiResult, getKotaResult,registerLoading } = this.props;

        console.log("Get Params :", this.props.route.params);

        return (
            <KeyboardAvoidingView behavior={Platform.os === "android" ? "padding" : "height"}
                style={styles.pages}>
                <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
                    <ScrollView>
                        <View style={styles.ilustrasi}>
                            <LogoRegister2 />
                            <Jarak height={5} />
                            <Text style={styles.title}>Daftar</Text>
                            <Text style={styles.title}>Lengkapi data diri anda</Text>

                            <View style={styles.wrapperCircle}>
                                <View style={styles.circleDisable}></View>
                                <Jarak width={10} />
                                <View style={styles.circlePrimary}></View>
                            </View>
                        </View>
                        <View style={styles.card}>
                            <Inputan label="Alamat" textarea
                                value={alamat}
                                onChangeText={(alamat) => this.setState({ alamat })} />

                            <Pilihan label="Provinsi"
                                datas={getProvinsiResult ? getProvinsiResult : []}
                                selectedValue={provinsi}
                                onValueChange={(provinsi) => this.ubahProvinsi(provinsi)}
                            />

                            <Pilihan label="Kota/Kab"
                                datas={getKotaResult ? getKotaResult : []}
                                selectedValue={kota}
                                onValueChange={(kota) => this.setState({ kota: kota })}
                            />

                            <Jarak height={25} />
                            <View style={styles.btn}>
                                <Tombol title="Daftar" type="text" padding={10} fontSize={18} 
                                onPress={() => this.onContinue()}
                                loading={registerLoading} />
                                <Jarak width={8} />
                                <Tombol title="Batal" type="text" padding={10} fontSize={18} onPress={() => this.props.navigation.goBack()} />
                            </View>
                        </View>
                    </ScrollView>
                </TouchableWithoutFeedback>
            </KeyboardAvoidingView>
        )
    }
}

const mapStatetoProps = (state) => ({
    getProvinsiResult: state.RajaOngkirReducer.getProvinsiResult,
    getKotaResult: state.RajaOngkirReducer.getKotaResult,

    registerLoading: state.AuthReducer.registerLoading,
    registerResult: state.AuthReducer.registerResult,
    registerError: state.AuthReducer.registerError,
})

export default connect(mapStatetoProps, null)(Register2)

const styles = StyleSheet.create({
    pages: {
        flex: 1,
        backgroundColor: colors.white
    },
    ilustrasi: {
        alignItems: 'center',
        marginTop: responsiveHeight(40)
    },
    title: {
        fontSize: 24,
        fontFamily: fonts.primary.light,
        color: colors.primary
    },
    wrapperCircle: {
        flexDirection: 'row',
        marginTop: 10
    },
    circlePrimary: {
        backgroundColor: colors.primary,
        width: responsiveWidth(11),
        height: responsiveWidth(11),
        borderRadius: 10
    },
    circleDisable: {
        backgroundColor: colors.border,
        width: responsiveWidth(11),
        height: responsiveWidth(11),
        borderRadius: 10
    },
    card: {
        backgroundColor: colors.white,
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
        paddingHorizontal: 30,
        paddingBottom: 10,
        paddingTop: 15,
        borderRadius: 5,
        marginTop: 30
    },
    btn: {
        flexDirection: 'row',
        alignItems: 'flex-end'
    }
    // btnBack: {
    //     marginLeft: 30,
    //     marginTop: responsiveHeight(40),
    //     position: 'absolute'
    // }
})