import React, { Component } from 'react'
import { Text, StyleSheet, View, KeyboardAvoidingView, TouchableWithoutFeedback, Keyboard, Platform, ScrollView, Alert } from 'react-native'
import { LogoRegister1, logoRegister1 } from '../../assets'
import { Inputan, Jarak, Tombol } from '../../components'
import { colors, fonts, responsiveHeight, responsiveWidth } from '../../utils'

export default class Register1 extends Component {

    constructor(props) {
        super(props)

        this.state = {
            nama: '',
            email: '',
            password: '',
            nohp: ''
        }
    }

    onContinue = () => {
        const { nama, email, password, nohp } = this.state
        if (nama && email && password && nohp) {
            this.props.navigation.navigate('Register2', this.state);
        } else {
            Alert.alert("Error", "Data belum lengkap");
        }
    }

    render() {

        const { nama, email, password, nohp } = this.state

        return (
            <KeyboardAvoidingView behavior={Platform.os === "android" ? "padding" : "height"}
                style={styles.pages}>
                <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
                    <ScrollView>
                        <View style={styles.ilustrasi}>
                            <LogoRegister1 />
                            <Jarak height={5} />
                            <Text style={styles.title}>Daftar</Text>
                            <Text style={styles.title}>Lengkapi data diri anda</Text>

                            <View style={styles.wrapperCircle}>
                                <View style={styles.circlePrimary}></View>
                                <Jarak width={10} />
                                <View style={styles.circleDisable}></View>
                            </View>
                        </View>
                        <View style={styles.card}>
                            <Inputan label="Nama Lengkap" value={nama} onChangeText={(nama) => this.setState({ nama })} />
                            <Inputan label="Email" value={email} onChangeText={(email) => this.setState({ email })} />
                            <Inputan label="No Handphone" keyboardType="number-pad" value={nohp} onChangeText={(nohp) => this.setState({ nohp })} />
                            <Inputan label="Password" secureTextEntry value={password} onChangeText={(password) => this.setState({ password })} />
                            <Jarak height={25} />
                            <View style={styles.btn}>
                                <Tombol title="Selanjutnya" type="text" padding={10} fontSize={18}
                                    onPress={() => this.onContinue()} />
                                <Jarak width={8} />
                                <Tombol title="Batal" type="text" padding={10} fontSize={18}
                                    onPress={() => this.props.navigation.goBack()} />
                            </View>
                        </View>
                    </ScrollView>
                </TouchableWithoutFeedback>
            </KeyboardAvoidingView>
        )
    }
}

const styles = StyleSheet.create({
    pages: {
        flex: 1,
        backgroundColor: colors.white
    },
    ilustrasi: {
        alignItems: 'center',
        marginTop: responsiveHeight(40)
    },
    title: {
        fontSize: 24,
        fontFamily: fonts.primary.light,
        color: colors.primary
    },
    wrapperCircle: {
        flexDirection: 'row',
        marginTop: 10
    },
    circlePrimary: {
        backgroundColor: colors.primary,
        width: responsiveWidth(11),
        height: responsiveWidth(11),
        borderRadius: 10
    },
    circleDisable: {
        backgroundColor: colors.border,
        width: responsiveWidth(11),
        height: responsiveWidth(11),
        borderRadius: 10
    },
    card: {
        //marginHorizontal:30,
        backgroundColor: colors.white,
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
        paddingHorizontal: 30,
        paddingBottom: 10,
        paddingTop: 15,
        borderRadius: 5,
        marginTop: 30,
        //marginBottom:10
    },
    btn: {
        flexDirection: 'row',
        alignItems: 'flex-end'
    }
    // btnBack: {
    //     marginLeft: 30,
    //     marginTop: responsiveHeight(40),
    //     position: 'absolute'
    // }
})