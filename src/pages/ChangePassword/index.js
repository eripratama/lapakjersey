import React, { Component } from 'react'
import { Text, StyleSheet, View, ScrollView, Image, Alert } from 'react-native'
import { dummyProfile } from '../../data'
import { colors, fonts, getData, responsiveHeight, responsiveWidth } from '../../utils'
import { Inputan, Pilihan, Tombol } from '../../components'
import { connect } from 'react-redux'
import { changePassword } from '../../actions/ProfileAction'

class ChangePassword extends Component {

    constructor(props) {
        super(props)

        this.state = {
            password: '',
            newPassword: '',
            newPasswordConfirmation: ''
        }
    }

    onSubmit = () => {
        const { password, newPassword, newPasswordConfirmation } = this.state;

        if (newPassword !== newPasswordConfirmation) {
            Alert.alert('Error', 'Konfirmasi password baru harus sama')
        } else if (password && newPassword && newPasswordConfirmation) {
            // get data email user dari localstorage
            getData('user').then((res) => {
                const parameter = {
                    email: res.email,
                    password: password,
                    newPassword: newPassword
                }
                this.props.dispatch(changePassword(parameter));
            })
        } else {
            Alert.alert('Error', 'Isian form ada yang kosong!!!')

        }
    }

    componentDidUpdate(prevProps) {
        const {changePasswordResult} = this.props
        if (changePasswordResult && prevProps.changePasswordResult !== changePasswordResult) {
            Alert.alert("Notifikasi","Berhasil ganti password")
            this.props.navigation.replace('MainApp');
        }
    }

    render() {
        const { password, newPassword, newPasswordConfirmation } = this.state
        const {changePasswordLoading} = this.props
        return (
            <View style={styles.pages}>
                <ScrollView showsVerticalScrollIndicator={false}>

                    <Inputan
                        label="Password Lama"
                        value={password}
                        onChangeText={(password) => this.setState({ password })}
                        secureTextEntry />

                    <Inputan
                        label="Password Baru"
                        secureTextEntry
                        value={newPassword}
                        onChangeText={(newPassword) => this.setState({ newPassword })}
                    />

                    <Inputan
                        label="Konfirmasi Password Baru"
                        secureTextEntry
                        value={newPasswordConfirmation}
                        onChangeText={(newPasswordConfirmation) => this.setState({ newPasswordConfirmation })}
                    />

                    <View style={styles.submit}>
                        <Tombol title="Ganti Password" type="text"
                            padding={responsiveHeight(15)}
                            fontSize={18}
                            onPress={() => this.onSubmit()}
                            loading={changePasswordLoading}
                        />
                    </View>
                </ScrollView>
            </View>
        )
    }
}

const mapStateToProps = (state) => ({
    changePasswordLoading: state.ProfileReducer.changePasswordLoading,
    changePasswordResult: state.ProfileReducer.changePasswordResult,
    changePasswordError: state.ProfileReducer.changePasswordError,
})

export default connect(mapStateToProps, null)(ChangePassword)

const styles = StyleSheet.create({
    pages: {
        flex: 1,
        backgroundColor: colors.white,
        paddingHorizontal: 30,
        paddingTop: 10,
        justifyContent: 'space-between'
    },
    submit: {
        flex: 1,
        marginVertical: 30
    }
})
