import Logo from './logo.svg'
import IlustrasiSplash from './ilustrasi_splash_screen.js'
import Slider1 from './slider1.png'
import Slider2 from './slider2.png'
import Profil from './profil.png'
import LogoRegister1 from './register1.js'
import LogoRegister2 from './register2.js'
import DefaultImage from './default.png'

export { Logo, IlustrasiSplash, Slider1, Slider2, Profil, LogoRegister1, LogoRegister2, DefaultImage }