import chelsea1 from './chelsea1.png'
import chelsea2 from './chelsea2.png'
import dortmund1 from './dortmund1.png'
import dortmund2 from './dortmund2.png'
import juve1 from './juve1.png'
import juve2 from './juve2.png'
import leicester1 from './leicester1.png'
import leicester2 from './leicester2.png'
import liverpool1 from './liverpool1.png'
import liverpool2 from './liverpool2.png'
import madrid1 from './madrid1.png'
import madrid2 from './madrid2.png'
import milan1 from './milan1.png'
import milan2 from './milan2.png'
import munchen1 from './munchen1.png'
import munchen2 from './munchen2.png'

export {
    chelsea1,
    chelsea2,
    dortmund1,
    dortmund2,
    juve1,
    juve2,
    leicester1,
    leicester2,
    liverpool1,
    liverpool2,
    madrid1,
    madrid2,
    milan1,
    milan2,
    munchen1,
    munchen2
}