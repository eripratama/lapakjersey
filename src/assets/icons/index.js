import IconHome from './home.svg'
import IconHomeAktif from './home-aktif.svg'
import IconJersey from './jersey.svg'
import IconJerseyAktif from './jersey-aktif.svg'
import IconProfil from './profil.svg'
import IconProfilAktif from './profil-aktif.svg'
import IconCart from './cart.svg'
import IconSearch from './search.svg'

import IconArrowRight from './arrow-right.svg'
import IconChangePassword from './change-password.svg'
import IconOrderHistory from './order-history.svg'
import IconSignOut from './sign-out.svg'
import IconUserEdit from './user-edit.svg'

import IconArrowLeft from './arrow-left.svg'
import IconShoppingCart from './shopping-cart.svg'

import IconDelete from './delete.svg'
export {
    IconHome,
    IconHomeAktif,
    IconJersey,
    IconJerseyAktif,
    IconProfil,
    IconProfilAktif,
    IconCart,
    IconSearch,
    
    IconArrowRight,
    IconChangePassword,
    IconOrderHistory,
    IconSignOut,
    IconUserEdit,

    IconArrowLeft,
    IconShoppingCart,
    IconDelete
}