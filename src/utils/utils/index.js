import { Dimensions } from "react-native"
import { heightMobileUI, widthMobileUI } from "../constant";

//export const windowWidth = Dimensions.get('window').width;
//export const windowHeight = Dimensions.get('window').height;

export const responsiveWidth = (width) => {
    //return windowWidth*width/widthMobileUI;
    return Dimensions.get('window').width*width/widthMobileUI;
}

export const responsiveHeight = (height) => {
    //return windowHeight*height/heightMobileUI;
    return Dimensions.get('window').height*height/heightMobileUI;
}

export const formatPrice = (x) => {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

export const numberWithCommas = (x) => {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}