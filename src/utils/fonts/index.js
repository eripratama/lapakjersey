export const fonts = {
    primary: {
        light:"PublicSans-Light",
        regular:"PublicSans-Reglar",
        semibold:"PublicSans-SemiBold",
        bold:"PublicSans-Bold"
    }
}