export const heightMobileUI = 896;
export const widthMobileUI = 414;

export const API_TIMEOUT = 120000;
export const API_KEY = "2440d98d26bca8ae0f15872379220d20"

export const API_RAJAONGKIR = "https://api.rajaongkir.com/starter/"
export const API_HEADER_RAJAONGKIR = {
    "key": API_KEY
}
export const API_HEADER_RAJAONGKIR_COST = {
    "key": API_KEY,
    'content-type' : 'application/x-www-form-urlencode'
}

export const ORIGIN_CITY = '387';

