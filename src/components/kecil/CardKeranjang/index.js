import React from 'react'
import { Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import { connect } from 'react-redux'
import { deleteKeranjang } from '../../../actions/KeranjangAction'
import { IconDelete } from '../../../assets'
import { colors, fonts, formatPrice, responsiveHeight, responsiveWidth } from '../../../utils'
import Jarak from '../Jarak'

const CardKeranjang = ({ keranjang, keranjangUtama, id, dispatch }) => {

    const hapusKeranjang = () => {
        dispatch(deleteKeranjang(id, keranjangUtama, keranjang))
    }
    return (
        <View style={styles.container}>
            <Image source={{ uri: keranjang.product.gambar[0] }} style={styles.gambar} />
            <View style={styles.desc}>
                <Text style={styles.nama}>{keranjang.product.nama}</Text>
                <Text style={styles.text}>Rp. {formatPrice(keranjang.product.harga)}</Text>
                {/* <Text style={styles.text}>Rp. {keranjang.product.harga}</Text> */}

                <Jarak height={responsiveHeight(14)} />

                <Text style={styles.textBold}>
                    <Text style={styles.text}>Jml Item : {keranjang.jumlahPesan}</Text>
                </Text>
                <Text style={styles.textBold}>
                    <Text style="styles.text">Size : {keranjang.ukuran}</Text>
                </Text>
                <Text style={styles.textBold}>
                    <Text style={styles.text}>Total Harga : Rp. {formatPrice(keranjang.totalHarga)}</Text>
                    {/* <Text style={styles.text}>Total Harga : Rp. {keranjang.totalHarga}</Text> */}
                </Text>
                <Text style={styles.textBold}>Keterangan: </Text>
                <Text style={styles.text}>{keranjang.keterangan}</Text>
            </View>
            <TouchableOpacity style={styles.hapus} onPress={() => hapusKeranjang()}>
                <IconDelete />
            </TouchableOpacity>
        </View>
    )
}

export default connect()(CardKeranjang)

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        marginTop: 15,
        backgroundColor: colors.white,
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
        marginHorizontal: 30,
        paddingHorizontal: 15,
        paddingVertical: 10,
        borderRadius: 15,
        alignItems: 'center'
    },
    gambar: {
        width: responsiveWidth(77),
        height: responsiveHeight(88),
        resizeMode: 'contain'
    },
    hapus: {
        flex: 1,
        alignItems: 'flex-end'
    },
    nama: {
        fontFamily: fonts.primary.bold,
        fontSize: 13,
        textTransform: 'capitalize'
    },
    text: {
        fontFamily: fonts.primary.regular,
        fontSize: 11,
        textTransform: 'capitalize'
    },
    textBold: {
        fontFamily: fonts.primary.bold,
        fontSize: 11,
        textTransform: 'capitalize'
    }
})
