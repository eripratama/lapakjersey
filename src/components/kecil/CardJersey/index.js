import React from 'react'
import { Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import { colors, fonts, responsiveHeight, responsiveWidth } from '../../../utils'
import Tombol from '../Tombol'

const CardJersey = ({ itemJersey, navigation }) => {
    return (
        <View style={styles.container}>
            <TouchableOpacity style={styles.card}>
                <Image style={styles.gambar} source={{uri:itemJersey.gambar[0]}} />
                <Text style={styles.title}>{itemJersey.nama}</Text>
            </TouchableOpacity>

            <Tombol type="text" title="Detail" padding={7} onPress={() => navigation.navigate('JerseyDetail',{itemJersey})} />
        </View>
    )
}

export default CardJersey

const styles = StyleSheet.create({
    container: {
        marginBottom: 25
    },
    gambar: {
        marginTop: 10,
        width: responsiveWidth(124),
        height: responsiveHeight(124)
    },
    title: {
        fontFamily: fonts.primary.bold,
        fontSize: 13,
        textTransform: 'capitalize',
        textAlign: 'center',
        padding: 10,
        borderRadius: 10,
        marginBottom: 10
    },
    card: {
        backgroundColor: colors.cardColor,
        width: responsiveWidth(150),
        alignItems: 'center'
    }
})
