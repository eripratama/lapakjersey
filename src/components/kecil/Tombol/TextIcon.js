import React from 'react'
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import { IconArrowLeft, IconCart } from '../../../assets'
import { colors, fonts } from '../../../utils'
import Jarak from '../Jarak'
import TextOnly from './TextOnly'

const TextIcon = ({ icon, padding, type, title, onPress, fontSize, disabled }) => {

    const Icon = () => {
        if (icon === 'cart') {
            return <IconCart />
        } else if (icon === 'arrow-left') {
            return <IconArrowLeft />
        }

        return <IconCart />
    }

    return (
        <TouchableOpacity style={styles.container(padding,disabled)} onPress={onPress}>
            <Icon />
            <Jarak width={5} />
            <Text style={styles.title(fontSize)}>{title}</Text>
        </TouchableOpacity>
    )
}

export default TextIcon

const styles = StyleSheet.create({
    container: (padding,disabled) => ({
        backgroundColor: disabled ? colors.border :  colors.primary,
        padding: padding,
        borderRadius: 5,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    }),
    title: (fontSize) => ({
        color: colors.white,
        fontSize: fontSize ? fontSize : 15,
        fontFamily: fonts.primary.bold
    })

})
