import React from 'react'
import { StyleSheet, Text, TextInput, View } from 'react-native'
import { colors, fonts } from '../../../utils'

const Inputan = ({ textarea, width, height, fontSize,
    placeholder, label, value, secureTextEntry, keyboardType, onChangeText, disabled }) => {

    if (textarea) {
        return (
            <View style={styles.container}>
                <Text style={styles.label(fontSize)}>{label} :</Text>
                <TextInput
                    style={styles.inputTextArea(fontSize)}
                    multiline={true}
                    numberOfLines={5}
                    placeholder={placeholder}
                    placeholderTextColor="#000"
                    value={value}
                    onChangeText={onChangeText}
                    editable={disabled ? false : true}
                />
            </View>
        )
    }

    return (
        <View style={styles.container}>
            <Text style={styles.label(fontSize)}>{label} :</Text>
            <TextInput style={styles.input(width, height, fontSize)} value={value}  editable={disabled ? false : true}
                secureTextEntry={secureTextEntry} keyboardType={keyboardType} onChangeText={onChangeText} />
        </View>
    )
}

export default Inputan

const styles = StyleSheet.create({
    container: {
        marginTop: 10
    },
    label: (fontSize) => ({
        fontSize: fontSize ? fontSize : 18,
        fontFamily: fonts.primary.regular
    }),
    input: (width, height, fontSize) => ({
        fontSize: fontSize ? fontSize : 18,
        fontFamily: fonts.primary.regular,
        width: width,
        height: height,
        borderWidth: 1,
        borderRadius: 5,
        borderColor: colors.border,
        paddingVertical: 5,
        paddingHorizontal: 10,
        color: 'black'
    }),
    inputTextArea: (fontSize) => ({
        fontSize: fontSize ? fontSize : 18,
        fontFamily: fonts.primary.regular,
        textAlignVertical: 'top',
        borderWidth: 1,
        borderRadius: 5,
        borderColor: colors.border,
        paddingVertical: 5,
        paddingHorizontal: 10,
        color: 'black'
    })
})
