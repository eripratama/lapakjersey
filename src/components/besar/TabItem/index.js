import React from 'react'
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import { IconHome, IconHomeAktif, IconJersey, IconJerseyAktif, IconProfil, IconProfilAktif } from '../../../assets'
import { colors, fonts } from '../../../utils'

const TabItem = ({ isFocused, onLongPress, onPress, label }) => {

    const Icon = () => {

        if (label === "Home") {
            return isFocused ? <IconHomeAktif /> : <IconHome />
        }

        if (label === "Jersey") {
            return isFocused ? <IconJerseyAktif /> : <IconJersey />
        }

        if (label === "Profile") {
            return isFocused ? <IconProfilAktif /> : <IconProfil />
        }

        return <IconHome />
    }

    return (
        <TouchableOpacity
            // accessibilityRole="button"
            // accessibilityState={isFocused ? { selected: true } : {}}
            onPress={onPress}
            onLongPress={onLongPress}
            style={styles.container}>
            <Icon />
            <Text style={styles.text(isFocused)}>
                {label}
            </Text>
        </TouchableOpacity>
    )
}

export default TabItem

const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        flex: 1
    },
    text: (isFocused) => ({
        color: isFocused ? colors.white : colors.secondary,
        fontSize: 11,
        marginTop: 4,
        fontFamily:fonts.primary.bold
    })
})
