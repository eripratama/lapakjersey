import React, { Component } from 'react'
import { Text, StyleSheet, View, TextInput } from 'react-native'
import { colors, fonts, getData, responsiveHeight } from '../../../utils'
import { IconSearch } from '../../../assets'
import Tombol from '../../kecil/Tombol'
import Jarak from '../../kecil/Jarak'
import { connect } from 'react-redux'
import { saveKeywordJersey } from '../../../actions/JerseyAction'
import { getListKeranjang } from '../../../actions/KeranjangAction'

class HeaderComponent extends Component {
    constructor(props) {
        super(props)

        this.state = {
            search: ""
        }
    }

    componentDidMount() {
        getData('user').then((res) => {
            if (res) {

                this.props.dispatch(getListKeranjang(res.uid))
            }
        })
    }

    selesaiCari = () => {
        console.log("Cek state cari", this.state.search);
        const { page, navigation, dispatch } = this.props;
        const { search } = this.state

        // jalankan action save keyword pencarian
        dispatch(saveKeywordJersey(search));

        // jika halaman home navigate ke halaman listjersey
        if (page !== "ListJersey") {
            navigation.navigate("ListJersey");
        }


        // kembalikan state search menjadi string kosong
        this.setState({
            search: ''
        })
    }

    render() {
        const { search } = this.state
        const { navigation, getListKeranjangResult } = this.props

        let totalKeranjang;
        if (getListKeranjangResult) {
            totalKeranjang = Object.keys(getListKeranjangResult.pesanans).length;
        }

        return (
            <View style={styles.container}>
                <View style={styles.wrapperHeader}>
                    <View style={styles.searchSection}>
                        <IconSearch />
                        <TextInput placeholder="Cari jersey..." placeholderTextColor="#000"
                            style={styles.input} value={search}
                            onChangeText={(search) => this.setState({ search })}
                            onSubmitEditing={() => this.selesaiCari()}
                        />
                    </View>
                    <Jarak width={10} />
                    <Tombol icon="cart" totalKeranjang={totalKeranjang} padding={10}
                        onPress={() => navigation.navigate('Keranjang')}
                    />
                </View>
            </View>
        )
    }
}

const mapStateToProps = (state) => ({
    getListKeranjangLoading: state.KeranjangReducer.getListKeranjangLoading,
    getListKeranjangResult: state.KeranjangReducer.getListKeranjangResult,
    getListKeranjangError: state.KeranjangReducer.getListKeranjangError,
})

export default connect(mapStateToProps, null)(HeaderComponent)

const styles = StyleSheet.create({
    container: {
        backgroundColor: colors.primary,
        height: responsiveHeight(125)
    },
    searchSection: {
        flex: 1,
        flexDirection: 'row',
        backgroundColor: colors.white,
        borderRadius: 5,
        paddingLeft: 10,
        alignItems: 'center'
    },
    wrapperHeader: {
        marginTop: 25,
        marginHorizontal: 30,
        flexDirection: 'row'
    },
    input: {
        fontSize: 16,
        color: 'black',
        fontFamily: fonts.primary.regular

    }
})
