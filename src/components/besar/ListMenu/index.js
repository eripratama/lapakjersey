import React from 'react'
import { View, Text } from 'react-native'
import { CardMenu } from '../../kecil'

const ListMenu = ({menus,navigation}) => {
    return (
        <View>
            {menus.map((menu) => {
                return <CardMenu menu={menu} key={menu.id} navigation={navigation}/>
            })}
            <Text></Text>
        </View>
    )
}

export default ListMenu
