import { BundesLiga, LaLiga, PremierLeague, SerieA } from "../../assets";


export const dummyLiga = [
  {
    id: 1,
    nama: 'La Liga',
    gambar: LaLiga,
  },
  {
    id: 2,
    nama: 'Premier League',
    gambar: PremierLeague,
  },
  {
    id: 3,
    nama: 'Serie A',
    gambar: SerieA,
  },
  {
    id: 4,
    nama: 'Bundesliga',
    gambar: BundesLiga,
  },
];
