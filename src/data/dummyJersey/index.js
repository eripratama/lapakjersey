import {
  BundesLiga,
    chelsea1,
    chelsea2,
    dortmund1,
    dortmund2,
    juve1,
    juve2,
    LaLiga,
    leicester1,
    leicester2,
    liverpool1,
    liverpool2,
    madrid1,
    madrid2,
    milan1,
    milan2,
    munchen1,
    munchen2,
    PremierLeague,
    SerieA
} from '../../assets'

export const dummyJersey = [
    {
        id: 1,
        nama: 'CHELSEA 3RD 2018-2019',
        gambar: [chelsea1, chelsea2],
        liga: {
          id: 2,
          nama: 'Premier League',
          gambar: PremierLeague,
        },
        harga: 125000,
        berat: 0.25,
        jenis: 'Replika Top Quality',
        ukuran: ["S", "M", "L", "XL", "XXL"],
        ready: true
      },
      {
        id: 2,
        nama: 'DORTMUND AWAY 2018-2019',
        gambar: [dortmund1, dortmund2],
        liga: {
          id: 4,
          nama: 'Bundesliga',
          gambar: BundesLiga,
        },
        harga: 125000,
        berat: 0.25,
        jenis: 'Replika Top Quality',
        ukuran: ["S", "M", "L", "XL", "XXL"],
        ready: true
      },
      {
        id: 3,
        nama: 'JUVENTUS AWAY 2018-2019',
        gambar: [juve1, juve2],
        liga: {
          id: 3,
          nama: 'Serie A',
          gambar: SerieA,
        },
        harga: 125000,
        berat: 0.25,
        jenis: 'Replika Top Quality',
        ukuran: ["S", "M", "L", "XL", "XXL"],
        ready: true
      },
      {
        id: 4,
        nama: 'LEICESTER CITY HOME 2018-2019',
        gambar: [leicester1, leicester2],
        liga: {
          id: 2,
          nama: 'Premier League',
          gambar: PremierLeague,
        },
        harga: 125000,
        berat: 0.25,
        jenis: 'Replika Top Quality',
        ukuran: ["S", "M", "L", "XL", "XXL"],
        ready: true
      },
      {
        id: 5,
        nama: 'LIVERPOOL AWAY 2018-2019',
        gambar: [liverpool1, liverpool2],
        liga: {
          id: 2,
          nama: 'Premier League',
          gambar: PremierLeague,
        },
        harga: 125000,
        berat: 0.25,
        jenis: 'Replika Top Quality',
        ukuran: ["S", "M", "L", "XL", "XXL"],
        ready: true
      },
      {
        id: 6,
        nama: 'REAL MADRID 3RD 2018-2019',
        gambar: [madrid1, madrid2],
        liga: {
          id: 1,
          nama: 'La Liga',
          gambar: LaLiga,
        },
        harga: 125000,
        berat: 0.25,
        jenis: 'Replika Top Quality',
        ukuran: ["S", "M", "L", "XL", "XXL"],
        ready: true
      },
      {
        id: 7,
        nama: 'AC MILAN HOME 2018 2019',
        gambar: [milan1, milan2],
        liga: {
          id: 3,
          nama: 'Serie A',
          gambar: SerieA,
        },
        harga: 125000,
        berat: 0.25,
        jenis: 'Replika Top Quality',
        ukuran: ["S", "M", "L", "XL", "XXL"],
        ready: true
      },
      {
        id: 8,
        nama: 'BAYERN MUNCHEN 3RD 2018 2019',
        gambar: [munchen1, munchen2],
        liga: {
          id: 4,
          nama: 'Bundesliga',
          gambar: BundesLiga,
        },
        harga: 125000,
        berat: 0.25,
        jenis: 'Replika Top Quality',
        ukuran: ["S", "M", "L", "XL", "XXL"],
        ready: true
      },
];