import React from 'react';
import { IconChangePassword, IconOrderHistory, IconSignOut, IconUserEdit } from "../../assets/icons/index";

export const dummyMenu = [
    {
        id: 1,
        nama: 'Edit Profil',
        gambar: <IconUserEdit/>,
        halaman: 'EditProfile'
    },

    {
        id: 2,
        nama: 'Change Password',
        gambar: <IconChangePassword />,
        halaman: 'ChangePassword'
    },

    {
        id: 3,
        nama: 'History Pemesanan',
        gambar: <IconOrderHistory />,
        halaman: 'OrderHistory'
    },

    {
        id: 4,
        nama: 'Sign Out',
        gambar: <IconSignOut />,
        halaman: 'Login'
    }
    
];