import {
    chelsea1,
    chelsea2,
    PremierLeague
} from '../../assets'

export const dummyOrder = [
    {
        id: 1,
        tanggalOrder: 'Senin, 6 September 2021',
        status: 'keranjang',
        totalHarga: 125000,
        berat: 1,
        orders: [
            {
                id: 1,
                product: {
                    id: 1,
                    nama: 'CHELSEA 3RD 2018-2019',
                    gambar: [chelsea1, chelsea2],
                    liga: {
                        id: 2,
                        nama: 'Premier League',
                        gambar: PremierLeague,
                    },
                    harga: 125000,
                    berat: 0.25,
                    jenis: 'Replika Top Quality',
                    ukuran: ["S", "M", "L", "XL", "XXL"],
                    ready: true
                },
                jumlahOrder: 1,
                totalHarga: 125000,
                keterangan: "Nomor punggung 7 nama saya saja",
                ukuran: "L"
            }
        ]
    }
];