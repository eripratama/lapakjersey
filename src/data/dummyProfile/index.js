import { Profil } from '../../assets'

export const dummyProfile = {
    nama: 'Eri Pratama',
    email: 'eripratama@gmail.test',
    nomorHp: '085212345678',
    alamat: 'Jl Straat 1 No.46',
    kota: 'Samarinda',
    provinsi: 'Kaltim',
    avatar: Profil
}